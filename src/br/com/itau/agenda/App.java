package br.com.itau.agenda;

import java.awt.List;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {

		String nome;
		long telefone;
		Agenda agenda = new Agenda();
		Contato contato = new Contato();

		agenda.listaContato = new ArrayList<Contato>();

		Scanner scanner = new Scanner(System.in);

		System.out.println("Bem vindo ao sistema de agenda !!");
		System.out.println("Qual o nome da agenda ?");
		String nomeAgenda = scanner.nextLine();
		agenda = agenda.criarAgenda(nomeAgenda);
		if (agenda.agendaCriada) {
			
			agenda.listaContato = contato.criarContato(agenda);
		}

		System.out.println(agenda);
	}

}
