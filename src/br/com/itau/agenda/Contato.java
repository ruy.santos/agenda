package br.com.itau.agenda;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Contato {

	public String nome;

	public long telefone;
	public boolean contatoCriado;
//	public List<Telefone> listaTelefone;

	public List<Contato> criarContato(Agenda agenda) {
		Scanner scanner = new Scanner(System.in);
		Contato contato = new Contato();
		agenda.listaContato = new ArrayList<Contato>();
		boolean continuar = true;
		String novoContato = "SIM";
		
		
		while (continuar) {
			System.out.println("Qual o contato ?");
			contato.nome = scanner.nextLine();
			
			System.out.println("Qual o telefone do contato?");
			try {
				contato.telefone = Long.parseLong(scanner.nextLine());
			} catch (NumberFormatException e) {
				try {
					System.out.println("Numero incorreto, tente novamente !!");
					contato.telefone = Long.parseLong(scanner.nextLine());
				} catch (NumberFormatException f) {
					System.out.println("Burro !!");
				}
			}

			agenda.listaContato.add(contato);
			contato = new Contato();

			System.out.println("Deseja cadastrar mais um contato S/N ?");
			novoContato = scanner.nextLine();

			if (novoContato.equalsIgnoreCase("S") || novoContato.toLowerCase().equalsIgnoreCase("sim")) {
				continuar = true;
			} else {
				continuar = false;
			}
			contato.contatoCriado = true;
		}
		return agenda.listaContato;
	}
}
