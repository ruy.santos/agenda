package br.com.itau.agenda;

import java.util.List;

public class Agenda {

	public String proprietario;
	public List<Contato> listaContato;
	public boolean agendaCriada;
	
	public String toString () {
		System.out.println("Nome da Agenda .: " + proprietario);
		
		for (Contato contato: listaContato) {
			System.out.println("Nome :" + contato.nome);
			System.out.println("Telefone :" + contato.telefone);
			System.out.println("____________________________");
		}
		return "fim";
	}

	public Agenda criarAgenda(String nome) {
		Agenda agenda = new Agenda();
		agenda.proprietario = nome;
		agenda.agendaCriada = true;
		return agenda;
	}
}
